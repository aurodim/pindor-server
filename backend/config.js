require('dotenv').config();

module.exports = {
  secret: process.env.JWT_SECRET,
  twilioSid: process.env.TWILIO_SID,
  twilioAuth: process.env.TWILIO_AUTH,
  twilioNumber: process.env.TWILIO_NUMBER
};
