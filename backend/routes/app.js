const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = require('../controllers/user');
module.exports = router;

router.route('/api/register').post((req, res) => {
  if (!(req.body.firstName) || !(req.body.lastName) || !(req.body.phoneNumber) || !(req.body.password)) {
    return Respond(res, {_iE: true, _c: 401, _e: 'INEX_PARAMS'});
  }

  User.register(req.body).then(response => {
    return Respond(res, { ...response });
  });
});

router.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/../../public/index.html'));
});

function Respond(res, {_iE = false, _c = 200, _e = null, _m = null, _body = {}}) {
  res.status(_c).json({_iE, _c, _e, _m, _body});
}
