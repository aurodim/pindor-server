require('dotenv').config();
const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const compression = require('compression');
const mongoose = require('mongoose');
const appRoutes = require('./routes/app.js');
const PORT = 80;

function requireHTTPS(req, res, next) {
  // The 'x-forwarded-proto' check is for Heroku
  if (!req.secure && req.get('x-forwarded-proto') !== 'https' && process.env.NODE_ENV !== "development") {
    return res.redirect('https://' + req.get('host') + req.url);
  }
  next();
}

const allowedOrigins = [
  'file://',
  'capacitor://localhost',
  'ionic://localhost',
  'http://localhost',
  'http://localhost:8080',
  'http://localhost:8100',
  'http://localhost:4200',
  '*.forestadmin.com',
  'http://app.forestadmin.com',
  'https://app.forestadmin.com'
];

const corsOptions = {
  origin: (origin, callback) => {
    if (allowedOrigins.includes(origin) || !origin || process.env.NODE_ENV !== 'production') {
      callback(null, true);
    } else {
      callback(new Error('Origin not allowed by CORS'));
    }
  },
  allowedHeaders: ['Authorization', 'X-Requested-With', 'Content-Type'],
  credentials: true // This is important.
}

app.set('view cache', true);
app.use(requireHTTPS);
app.use(cookieParser(process.env.COOKIE_SECRET));
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '50mb'
}));
app.use(cors(corsOptions));
app.use(helmet({
  frameguard: {
    action: 'deny'
  }
}));
app.use(compression());
app.set('etag', false);

const options = {
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: true,
  useCreateIndex: true
};
mongoose.connect(process.env.MongoURI, options);
const connection = mongoose.connection;

connection.once('open', () => {
  console.log('MongoDB database connection established succesfully');
});

app.use('/', appRoutes);

app.listen(process.env.PORT || PORT, () => {
  console.log('Server is running on port: ' + PORT);
});
