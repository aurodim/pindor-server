const crypto = require('crypto');
const User = require('../models/user');
const config = require('../config');
const twilio = require('twilio')(config.twilioSid, config.twilioAuth);

module.exports.register = Register;

async function Register(body) {
  const firstName = body.firstName;
  const lastName = body.lastName;
  const phoneNumber = body.phoneNumber.replace(/\D/g,'');
  const password = body.password;

  const key = crypto.randomBytes(3).toString("hex") + "." + crypto.randomBytes(7).toString("hex") + "." + crypto.randomBytes(5).toString("hex");
  const socket = crypto.randomBytes(2).toString("hex") + "." + crypto.randomBytes(40).toString("hex") + "." + crypto.randomBytes(1).toString("hex");

  if (password.length < 4) {
    return respond({
      isError: true,
      error: 'PASS_LEN'
    });
  }

  try {
    const user = new User({
      crypto : key,
      socket : socket,
      firstName : firstName,
      lastName : lastName,
      phoneNumber : phoneNumber
    });
    user.setPassword(password);
    const savedUser = await user.save();
    twilio.messages.create({
      body: `Welcome to Pindor, ${firstName}! Checkout our website for more information on how our platform works (https://www.pindor.io), enjoy!`,
      from: config.twilioNumber,
      to: phoneNumber
    });
    return respond({
      body: savedUser.toAuth()
    });
  } catch (e) {
    let err = 'SERVER_ERR';

    if ('phoneNumber' in e.errors) {
      err = "DUP_NUMBER";
    }
    return respond({
      isError: true,
      code: 500,
      error: err,
    });
  }
}

function respond({isError = false, code = 200, error = null, message = isError ? 'API encountered error' : 'API finished task successfully', body = {}}) {
  return {
    _iE: isError,
    _c: code,
    _e: error,
    _m: message,
    _body: body
  }
}
