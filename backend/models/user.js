const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const secret = require('../config').secret;
const Schema = mongoose.Schema;

const schema = new Schema({
  crypto: {type: String, required: true, unique: true},
  socket: {type: String, required: true, unique: true},
  firstName: {type: String, required: true, unique: false},
  lastName: {type: String, required: true, unique: false},
  phoneNumber: {type: String, required: true, unique: true},
  businesses: [{ type: Schema.Types.ObjectId, ref: 'Business' }],
  password: String,
  salt: String
}, {timestamps: true});

schema.methods.setPassword = (password) => {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.password = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};
schema.methods.validPassword = (password) => {
 const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
 return this.password === hash;
};
schema.methods.generateJWT = () => {
  var exp = moment().add(1, 'y');

  return jwt.sign({
    crypto: this.crypto,
    fullName: this.firstName + ' ' + this.lastName,
    exp: exp.unix()
  }, secret);
};
schema.methods.toAuth = () => {
  return {
    phoneNumber: this.phoneNumber,
    socket: this.socket,
    _auth: this.generateJWT(),
  };
};

schema.plugin(mongooseUniqueValidator);
module.exports = mongoose.model('User', schema);
