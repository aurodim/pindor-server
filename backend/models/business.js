const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const moment = require('moment');
const Schema = mongoose.Schema;

const locationSchema = new Schema({
  _id: {type: Schema.Types.ObjectId, index: true, required: true, auto: true},
  formatted : {type : String, unique : false, required : true},
  geo : { type: [Number], default: [0,0]}
});

const schema = new Schema({
  category: {type: String, required: true, unique: false},
  name: {type: String, required: true, unique: false},
  locations: [locationSchema],
  reviews: [{ type: Schema.Types.ObjectId, ref: 'Review' }],
  membership: {
    tier: {type: String, required: true},
    start: {type: Date, required: true},
    end: {type: Date, required: true}
  }
}, {timestamps: true});

schema.plugin(mongooseUniqueValidator);
module.exports = mongoose.model('Business', schema);
