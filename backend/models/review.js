const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const moment = require('moment');
const Schema = mongoose.Schema;

const schema = new Schema({
  author: {type: Schema.Types.ObjectId, ref: 'User'},
  message: {type: String, unique: false, required: true},
  rating: {type: Number, min: 0, max: 5}
}, {timestamps: true});

schema.plugin(mongooseUniqueValidator);
module.exports = mongoose.model('Review', schema);
