import React from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from './components/navbar';
import Landing from './components/landing';
import Explanation from './components/how-it-works';
import Pricing from './components/pricing';
import StoreBanner from './components/store';
import Footer from './components/footer';

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <NavBar></NavBar>
        <Landing></Landing>
        <Explanation></Explanation>
        <Pricing></Pricing>
        <StoreBanner></StoreBanner>
        <Footer></Footer>
      </React.Fragment>
    )
  }
}

export default App;
